local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")
local Button = require("objects/Button")

local About = {}

function About:load()
  About.buttons = {}
  table.insert(self.buttons, Button.new(
    "Back",
    function ()
      SM:setScene("Menu")
    end
  ))

  table.insert(self.buttons, Button.new(
    "Help me",
    function ()
      os.execute("start http://google.com")
    end
  ))

end

function About:update(dt)

end

function About:draw()
  local margin = 16
  local cursor_y = 0

  for i,v in ipairs(self.buttons) do
    v.width = WIDTH/2.5
    v.height = 60
    local total_height = (v.height + margin) * #self.buttons
    v.x = WIDTH/2 - v.width/2
    v.y = HEIGHT/2 - total_height/2 + cursor_y
    cursor_y = cursor_y + (v.height + margin)
    v:draw()
  end
end

function About:mousereleased(x, y, button, istouch, presses)
  for i,v in ipairs(self.buttons) do
    v:mousereleased(x, y, button, istouch, presses)
  end
end

return About
