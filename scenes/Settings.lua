local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")
local Button = require("objects/Button")

--[[ 
  This view is use for the main settings and game settings. 
]]

local Settings = {}

function Settings:load()
  Settings.buttons = {}
  table.insert(self.buttons, Button.new(
    "Back",
    function ()
      if PAUSE == true then
        PAUSE = false -- trigger for stop pause
      else
        SM:setScene("Menu")
        PAUSE = false
      end
    end
  ))

  table.insert(self.buttons, Button.new(
    "Sound",
    function ()

    end
  ))

  table.insert(self.buttons, Button.new(
    "Music",
    function ()
    
    end
  ))

  if PAUSE == true then -- if in game
    table.insert(self.buttons, Button.new(
      "Main Menu",
      function ()
        PAUSE = false
        SM:setScene("Menu")
      end
    ))
  end
end

function Settings:update(dt)

end

function Settings:draw()
  local margin = 16
  local cursor_y = 0

  if PAUSE == true then -- if in game
    -- draw back menu
    love.graphics.setColor(0,0,0, 0.90)
    love.graphics.rectangle(
      "fill",
      WIDTH - WIDTH/1.2, HEIGHT/25 * 4,
      WIDTH/1.5, HEIGHT/1.5 
    )
    love.graphics.setColor(1,1,1)
  end

  for i,v in ipairs(self.buttons) do
    -- change button style
    if PAUSE == true then
      v.textColor = { 1,1,1,1 }
      v.outlineColor = { 1,1,1,1 }
    end

    v.width = WIDTH/2.5
    v.height = 60
    local total_height = (v.height + margin) * #self.buttons
    v.x = WIDTH/2 - v.width/2
    v.y = HEIGHT/2 - total_height/2 + cursor_y
    cursor_y = cursor_y + (v.height + margin)
    v:draw()
  end
end

function Settings:mousereleased(x, y, button, istouch, presses)
  for i,v in ipairs(self.buttons) do
    v:mousereleased(x, y, button, istouch, presses)
  end
end

return Settings
