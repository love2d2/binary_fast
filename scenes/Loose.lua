local FM = require("tools/FontManager")
local CM = require("tools/CollisionManager")
local Button = require("objects/Button")

--[[ 
  This view is use in game when we win or loose. 
]]

local Loose = {}

function Loose:load()
  Loose.buttons = {}
  table.insert(self.buttons, Button.new(
    "Try again",
    function ()
      SM:setScene("Game")
      LOOSE = false
      PAUSE = false
    end
  ))

  table.insert(self.buttons, Button.new(
    "Sound",
    function ()

    end
  ))

  table.insert(self.buttons, Button.new(
    "Music",
    function ()
    
    end
  ))

  table.insert(self.buttons, Button.new(
    "Main Menu",
    function ()
      LOOSE = false
      PAUSE = false
      SM:setScene("Menu")
    end
  ))

end

function Loose:update(dt)

end

function Loose:draw()
  local margin = 16
  local cursor_y = 0

  -- draw back menu
  love.graphics.setColor(0,0,0, 0.90)
  love.graphics.rectangle(
    "fill",
    WIDTH - WIDTH/1.2, HEIGHT/25 * 4,
    WIDTH/1.5, HEIGHT/1.5 
  )
  love.graphics.setColor(1,1,1)

  for i,v in ipairs(self.buttons) do
    -- change button style
    v.textColor = { 1,1,1,1 }
    v.outlineColor = { 1,1,1,1 }
    v.width = WIDTH/2.5
    v.height = 60
    local total_height = (v.height + margin) * #self.buttons
    v.x = WIDTH/2 - v.width/2
    v.y = HEIGHT/2 - total_height/2 + cursor_y
    cursor_y = cursor_y + (v.height + margin)
    v:draw()
  end
end

function Loose:mousereleased(x, y, button, istouch, presses)
  for i,v in ipairs(self.buttons) do
    v:mousereleased(x, y, button, istouch, presses)
  end
end

return Loose
