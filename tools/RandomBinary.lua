return {
  randomCode = function(length)
    local num = ""
    for i=1,length do
      num = num..tostring(math.floor(love.math.random( 0, 1 )))
    end
    return num
  end,

  calculResponse = function(str)
    local bufferResponse = ""
    for c in tostring(str):gmatch"." do
      if(c == "1") then bufferResponse = bufferResponse.."0" end
      if(c == "0") then bufferResponse = bufferResponse.."1" end
    end
    return tostring(bufferResponse)
  end,

  isEqual = function(userResponse, s2)
    local userStrLen = string.len(userResponse)
    if userStrLen ~= 0 then
      local currentCode = string.sub(s2, 0, userStrLen)
      if userResponse == currentCode then
        return true
      else
        return false
      end
    end
  end
}
