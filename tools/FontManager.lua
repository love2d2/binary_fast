local FontManager = {}

FontManager.title = love.graphics.newFont("assets/fonts/mh.ttf", 60)
FontManager.code = love.graphics.newFont("assets/fonts/mh.ttf", 40)
FontManager.main = love.graphics.newFont("assets/fonts/mh.ttf", 30)
FontManager.small =  love.graphics.newFont("assets/fonts/mh.ttf", 18)

return FontManager
