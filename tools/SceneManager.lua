local FM = require("tools/FontManager")

local Menu = require('scenes/Menu')
local Game = require('scenes/Game')
local Settings = require('scenes/Settings')
local About = require('scenes/About')

--[[
  load()                : initialize the first scene
  setScene(name)        : use to change the current scene
  update(dt)            : update the current scene
  draw()                : draw the current scene
  mousereleased(...)    : mousereleased callback function

  Accept values for name : Menu, Game, About, Settings
]]

local SceneManager = {}

function SceneManager:load(name)
  local init = name or "Menu"
  self.list = {
    Menu = Menu,
    Game = Game,
    Settings = Settings,
    About = About,
  }
  self.actual = self.list[init]
  self.actual:load()
end

function SceneManager:setScene(name)
  self.actual = self.list[name]
  self.actual:load()
end

function SceneManager:update(dt)
  self.actual:update(dt)
end

function SceneManager:draw()
  self.actual:draw()
end

function SceneManager:mousereleased(x, y, button, istouch, presses)
  self.actual:mousereleased(x, y, button, istouch, presses)
end

return SceneManager
