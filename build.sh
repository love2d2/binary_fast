# prerequisites : zip, apktool_2.4.1, uber-apk-signer-1.1.0

# make love file
zip -r build.zip ./* -x build.sh
mv build.zip game.love

# build apk
cp game.love ../build-android/love_decoded/assets
cd ../build-android/
java -version
java -jar apktool_2.4.1.jar b -o binary.apk love_decoded

# sign apk
java -jar uber-apk-signer-1.1.0.jar --apks binary.apk
