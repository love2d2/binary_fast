local FM = require("tools/FontManager")

return {
  new = function (min, max, value, x, y, width, height)
    local obj = {}
    obj.max = max or 100
    obj.min = min or 0
    obj.value = value or 0
    obj.width = width or WIDTH/1.5
    obj.height = height or HEIGHT / 25
    obj.x = x or WIDTH/2 - obj.width/2
    obj.y = y or 2*(HEIGHT / 25) 

    function obj:load()

    end

    function obj:update(dt)
      if self.value >= self.max then
        self.value = self.max
      elseif  self.value <= self.min then
        self.value = self.min
      end
    end

    function obj:redimention(newValue)
      self.max = newValue
      self.value = newValue
    end

    function obj:draw()
      -- draw back
      love.graphics.setColor(0.3,0.3,0.3,1)
      love.graphics.rectangle("fill", self.x, self.y, self.width, self.height)
      
      -- draw front
      local state = (self.value * self.width) / self.max
      love.graphics.setColor(0.4,1,0.4,0.7)
      if state -8 >= 0 then 
        love.graphics.rectangle(
          "fill", 
          self.x + 4, self.y + 4,
          state - 8, self.height - 8
        )
      end

      -- draw number
      --[[
      love.graphics.setColor(1,1,1,1)
      local timeW = FM.main:getWidth(self.value)
      local timeH = FM.main:getHeight(self.value)
      love.graphics.print(
        self.value,
        FM.main,
        self.x + self.width / 2 - timeW/2,
        self.y + self.height / 2 - timeH/2
      )
      ]]
      
      love.graphics.setColor(0,0,0,1)
    end

    return obj
  end
}
